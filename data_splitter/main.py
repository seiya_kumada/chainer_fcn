#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import random

DATA_FILE_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/trainval.txt"
TRAINING_DATA_FILE_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/training.txt"
TESTING_DATA_FILE_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/testing.txt"

TRAINING_RATE = 4
TESTING_RATE = 1


def save(ns, path):
    with open(path, "w") as f:
        f.write("\n".join(ns) + "\n")

if __name__ == "__main__":

    total_count = sum([1 for _ in open(DATA_FILE_PATH)])
    training_count = TRAINING_RATE * total_count / (TRAINING_RATE + TESTING_RATE)
    testing_count = total_count - training_count
    print(training_count, testing_count)
    names = [line.strip() for line in open(DATA_FILE_PATH)]
    random.shuffle(names)
    print(len(names))
    save(names[:training_count], TRAINING_DATA_FILE_PATH)
    save(names[training_count:], TESTING_DATA_FILE_PATH)

