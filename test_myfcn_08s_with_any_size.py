#!/usr/bin/env python
# coding:utf-8

import unittest
from myfcn_08s_with_any_size import MyFcn08sWithAnySize 
import chainer
import numpy as np
from chainer import gradient_check
import chainer.functions as F
import cv2
import os

class TestMyFcn08sWithAnySize(unittest.TestCase):
    def test_init(self):
        myfcn_08s_with_any_size = MyFcn08sWithAnySize().to_gpu() 
        self.assertNotEqual(myfcn_08s_with_any_size, None)

    def test_call(self):
        batch_size = 5 
        channel = 3
        row = 319 
        col = 513 
        x = chainer.Variable(chainer.cuda.to_gpu(np.ones((batch_size, channel, row, col)).astype(np.float32)))
        t = chainer.Variable(chainer.cuda.to_gpu(np.ones((batch_size, row, col)).astype(np.int32)))
        myfcn_08s_with_any_size = MyFcn08sWithAnySize().to_gpu()
        myfcn_08s_with_any_size.train = True 
        r = myfcn_08s_with_any_size(x, t)
        #print(r.data)
        self.assertEqual(myfcn_08s_with_any_size.p3_shape, (batch_size, MyFcn08sWithAnySize.CLASSES, row/8+1, col/8+1))
        self.assertEqual(myfcn_08s_with_any_size.p4_shape, (batch_size, MyFcn08sWithAnySize.CLASSES, row/16+1, col/16+1))
        self.assertEqual(myfcn_08s_with_any_size.p5_shape, (batch_size, MyFcn08sWithAnySize.CLASSES, row/32+1, col/32+1))
        self.assertEqual(myfcn_08s_with_any_size.u5_shape, (batch_size, MyFcn08sWithAnySize.CLASSES, row/16+1, col/16+1))
        self.assertEqual(myfcn_08s_with_any_size.pre_final_shape, (batch_size, MyFcn08sWithAnySize.CLASSES, row/8+1, col/8+1))
        self.assertEqual(myfcn_08s_with_any_size.final_shape, (batch_size, MyFcn08sWithAnySize.CLASSES, row, col))

    def make_labels(self, bs):
        l = np.array([
            [0, 1],
            [0, 1],
            [0, 1],
            ])
        r = np.zeros((bs, l.shape[0], l.shape[1])).astype(np.int32)
        r[:] = l
        return r 

    def make_h(self, bs):
        r0 = np.zeros((3, 2))
        r0[0,0] = 0.1
        r0[1,0] = 0.1
        r0[2,0] = 0.1
        #print(r0) 
        r1 = np.zeros((3, 2))
        r1[0,1] = 0.1
        r1[1,1] = 0.1
        r1[2,1] = 0.1
        #print(r1)
        classes = 2
        y = np.zeros((bs, classes, r0.shape[0], r0.shape[1])).astype(np.float32)
        y[:,0] = r0
        y[:,1] = r1
        return y

    def test_softmax_cross_entropy(self):
        batch_size = 4 
        labels = self.make_labels(batch_size)
        sh = self.make_h(batch_size)
        #print(h)
        h = chainer.Variable(chainer.cuda.to_gpu(sh))
        t = chainer.Variable(chainer.cuda.to_gpu(labels))
        r = F.softmax_cross_entropy(h, t)
        #print(r.data)
        self.assertAlmostEqual(0.644396662712, r.data) 
        h = chainer.Variable(chainer.cuda.to_gpu(np.argmax(sh, axis=1)))
        self.assertEqual((4, 3, 2), h.data.shape)
        self.assertEqual((4, 3, 2), t.data.shape)
        
        s = (h.data == t.data).mean(axis=1).mean(axis=1)
        self.assertEqual(s[0], 1) 
        self.assertEqual(s[1], 1) 
        self.assertEqual(s[2], 1) 
        self.assertEqual(s[3], 1) 

    def test_softmax_cross_entropy_3(self):
        batch_size = 4 
        h = self.make_h(batch_size)
        l = self.make_labels(batch_size)
        h = chainer.Variable(chainer.cuda.to_gpu(h))
        l = chainer.Variable(chainer.cuda.to_gpu(l))
        
        ch = chainer.cuda.cupy.argmax(h.data, axis=1)
        s = (ch == l.data).mean(axis=1).mean(axis=1).mean()
        self.assertEqual(1, s) 

    def test_softmax_cross_entropy_4(self):
        batch_size = 4 
        raw_h = self.make_h(batch_size)
        raw_l = self.make_labels(batch_size)
        raw_l[:,0,0] = -1
        gpu_h = chainer.Variable(chainer.cuda.to_gpu(raw_h))
        gpu_l = chainer.Variable(chainer.cuda.to_gpu(raw_l))

        cpu_h = chainer.cuda.to_cpu(gpu_h.data)
        cpu_l = chainer.cuda.to_cpu(gpu_l.data)
        mask = cpu_l != -1 
        reduced_cpu_h = np.argmax(cpu_h, axis=1)
        masked_reduced_cpu_h = reduced_cpu_h[mask]
        masked_cpu_l = cpu_l[mask]
        s = (masked_reduced_cpu_h == masked_cpu_l).mean()
        self.assertEqual(1, s) 

    def file_path_generator(self, src_path, dst_path):
        for p in os.listdir(src_path):
            yield (os.path.join(src_path, p), os.path.join(dst_path, p))

    def test_upsample_bilinearly(self):
        dpath = "/home/ubuntu/data/sbd/unit_test/inputs_any_size/"
        odpath = "/home/ubuntu/data/sbd/unit_test/outputs"

        # eight times
        in_channels = 3
        out_channels = 3
        ksize = 16 
        stride = 8 

        w = MyFcn08sWithAnySize.make_bilinear_interpolation_kernel(in_channels, out_channels, ksize=ksize)
        vw = chainer.Variable(chainer.cuda.to_gpu(w))

        for sfp, dfp in self.file_path_generator(dpath, odpath):
            print("\n{p}".format(p=sfp))
            img = cv2.imread(sfp).transpose(2, 0, 1).astype(np.float32)
            src_row = img.shape[1]
            src_col = img.shape[2]
            dst_row = stride * src_row - 1 
            dst_col = stride * src_col - 1 
            print("src image size: ({f}, {r}, {c})".format(f=img.shape[0], r=src_row, c=src_col))
            print("dst image size: ({f}, {r}, {c})".format(f=img.shape[0], r=dst_row, c=dst_col))
            img = img[np.newaxis,]
            var = chainer.Variable(chainer.cuda.to_gpu(img))

            row_pad = MyFcn08sWithAnySize.calculate_pad(src_row, dst_row, stride, ksize)
            col_pad = MyFcn08sWithAnySize.calculate_pad(src_col, dst_col, stride, ksize)
            print("(row_pad, col_pad) = ({r}, {c}) ".format(r=row_pad, c=col_pad))

            dst = F.deconvolution_2d(var, vw, stride=stride, pad=(row_pad, col_pad), outsize=(dst_row, dst_col))
            
            oi = chainer.cuda.to_cpu(dst.data[0].transpose(1, 2, 0).astype(np.uint8))
            print("final img: ({f}, {r}, {c})".format(f=oi.shape[2], r=oi.shape[0], c=oi.shape[1]))
            cv2.imwrite(dfp, oi)

if __name__ == "__main__":
    unittest.main()

