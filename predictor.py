#!/usr/bin/env python
# -*- coding: utf-8 -*-

from chainer import cuda, optimizers, Variable
import cPickle as pickle
from myfcn_32s_with_any_size import MyFcn32sWithAnySize
from mini_batch_loader.mini_batch_loader_with_any_size import * 
from chainer import cuda, optimizers, Variable
import chainer
import sys
import numpy as np
from label_maker.voc_label_colormap import make_color_map 
import cv2
import os
import shutil

FINE_TUNED_MODEL_PATH       = "/home/ubuntu/results/voc2012/2016-07-17/voc_myfcn_16s_with_any_size_60.pkl" 
TRAINING_DATA_PATH          = "/home/ubuntu/data/voc_2012/Segmentation/training.txt"
TESTING_DATA_PATH           = "/home/ubuntu/data/voc_2012/Segmentation/testing.txt_original"
IMAGE_DIR_PATH              = "/home/ubuntu/data/voc_2012/JPEGImages"
SEGMENTATION_CLASS_DIR_PATH = "/home/ubuntu/data/voc_2012/SegmentationClass"
TESTING_OUTPUT_DATA_DIR_PATH        = "/home/ubuntu/results/voc2012/2016-07-17/testing_outputs/predictions"
TESTING_GROUND_TRUTY_DATA_DIR_PATH  = "/home/ubuntu/results/voc2012/2016-07-17/testing_outputs/groundtruths"
TRAINING_OUTPUT_DATA_DIR_PATH        = "/home/ubuntu/results/voc2012/2016-07-17/training_outputs/predictions"
TRAINING_GROUND_TRUTY_DATA_DIR_PATH  = "/home/ubuntu/results/voc2012/2016-07-17/training_outputs/groundtruths"
INPUT_SCALE = 1


def check_label(m):
    a = set()
    for v in m.flatten():
        a.add(v)
    print(a)


def convert_to_images(label, n, index, accuracy, output_data_dir_path):
    
    label = chainer.cuda.cupy.argmax(label.data, axis=1)
    label = chainer.cuda.to_cpu(label)
    color_map = make_color_map()
    _, row, col = label.shape
    dst = np.ones((row, col, 3))
    l = label[0]
    for i in range(MyFcn32sWithAnySize.CLASSES):
        dst[l == i] = color_map[i] 
    basename = os.path.basename(n.label_path)
    dst_path = os.path.join(output_data_dir_path, basename)
    cv2.imwrite(dst_path, dst) 
    print("src = {s}, dst = {d}, accuracy = {a}".format(s=n.image_path, d=dst_path, a=accuracy))
    sys.stdout.flush()


def predict_data(name, raw_x, raw_t, model, index, output_dir, output_data_dir_path):
    shutil.copy(name.label_path, output_dir)
    x = chainer.Variable(chainer.cuda.to_gpu(raw_x))
    t = chainer.Variable(chainer.cuda.to_gpu(raw_t))
    model.train = False
    prediction = model(x, t)
    convert_to_images(prediction, name, index, model.accuracy.data, output_data_dir_path)
    sys.stdout.flush()


if __name__ == '__main__':
    # load a fine-tuned model 
    predictor = pickle.load(open(FINE_TUNED_MODEL_PATH)) 

    # load dataset 
    mini_batch_loader = MiniBatchLoaderWithAnySize(
        TRAINING_DATA_PATH, 
        TESTING_DATA_PATH, 
        IMAGE_DIR_PATH, 
        SEGMENTATION_CLASS_DIR_PATH, 
        MiniBatchLoaderWithAnySize.VOC)

    # predict
    for i in range(20):
        # testing data
        name = mini_batch_loader.testing_path_infos[i]
        x, t = mini_batch_loader.load_testing_data(i, INPUT_SCALE)
        predict_data(name, x, t, predictor, i, TESTING_GROUND_TRUTY_DATA_DIR_PATH, TESTING_OUTPUT_DATA_DIR_PATH)
        # training data
#        name = mini_batch_loader.training_path_infos[i]
#        x, t = mini_batch_loader.load_training_data(i, INPUT_SCALE)
#        predict_data(name, x, t, predictor, i, TRAINING_GROUND_TRUTY_DATA_DIR_PATH, TRAINING_OUTPUT_DATA_DIR_PATH)

