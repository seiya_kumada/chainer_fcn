#!/usr/bin/env python
# coding:utf-8

import unittest
from add import add, add2
import chainer
import numpy as np
from chainer import gradient_check
import chainer.functions as F

class TestAdd(unittest.TestCase):
    
    def test_add_forward_cpu(self):
        gx = chainer.Variable(np.array([[1, 2, 3]]).astype(np.float32))
        gy = chainer.Variable(np.array([[5, 4, 3]]).astype(np.float32))
        gz = chainer.Variable(np.array([[1, 1, 1]]).astype(np.float32))
        w = add(gx, gy, gz)
        g = np.array([[7.0, 7.0, 7.0]]).astype(np.float32)
        gradient_check.assert_allclose(g, w.data)
        
        w = add2(gx, gy)
        g = np.array([[6.0, 6.0, 6.0]]).astype(np.float32)
        gradient_check.assert_allclose(g, w.data)

    def test_add_forward_gpu(self):
        gx = chainer.Variable(chainer.cuda.to_gpu(np.array([[1, 2, 3]]).astype(np.float32)))
        gy = chainer.Variable(chainer.cuda.to_gpu(np.array([[5, 4, 3]]).astype(np.float32)))
        gz = chainer.Variable(chainer.cuda.to_gpu(np.array([[1, 1, 1]]).astype(np.float32)))
        w = add(gx, gy, gz)
        g = np.array([[7.0, 7.0, 7.0]]).astype(np.float32)
        gradient_check.assert_allclose(g, w.data)

        w = add2(gx, gy)
        g = np.array([[6.0, 6.0, 6.0]]).astype(np.float32)
        gradient_check.assert_allclose(g, w.data)

    def test_add_backward_cpu(self):
        x = chainer.Variable(np.random.randn(3, 2).astype(np.float32))
        y = chainer.Variable(np.random.randn(3, 2).astype(np.float32))
        z = chainer.Variable(np.random.randn(3, 2).astype(np.float32))
        w = add(x, y, z) 
        w.grad = np.ones((3, 2), dtype=np.float32)
        w.backward()

        f = lambda:(add(x, y, z).data,)
        gx, gy, gz = gradient_check.numerical_grad(f, (x.data, y.data, z.data), (w.grad,))
        gradient_check.assert_allclose(gx, x.grad)
        gradient_check.assert_allclose(gy, y.grad)
        gradient_check.assert_allclose(gz, z.grad)

    def test_add2_backward_cpu(self):
        x = chainer.Variable(np.random.randn(3, 2).astype(np.float32))
        y = chainer.Variable(np.random.randn(3, 2).astype(np.float32))
        w = add2(x, y) 
        w.grad = np.ones((3, 2), dtype=np.float32)
        w.backward()

        f = lambda:(add2(x, y).data,)
        gx, gy = gradient_check.numerical_grad(f, (x.data, y.data), (w.grad,))
        gradient_check.assert_allclose(gx, x.grad)
        gradient_check.assert_allclose(gy, y.grad)

    def test_add_backward_gpu(self):
        x = chainer.Variable(chainer.cuda.to_gpu(np.ones((3, 2), dtype=np.float32)))
        y = chainer.Variable(chainer.cuda.to_gpu(np.ones((3, 2), dtype=np.float32)))
        z = chainer.Variable(chainer.cuda.to_gpu(np.ones((3, 2), dtype=np.float32)))
        w = add(x, y, z) 
        w.grad = chainer.cuda.to_gpu(np.ones((3, 2), dtype=np.float32))
        w.backward()

        f = lambda:(add(x, y, z).data,)
        gx, gy, gz = gradient_check.numerical_grad(f, (x.data, y.data, z.data), (w.grad,))
        gradient_check.assert_allclose(gx, x.grad)
        gradient_check.assert_allclose(gy, y.grad)
        gradient_check.assert_allclose(gz, z.grad)

    def test_add2_backward_gpu(self):
        x = chainer.Variable(chainer.cuda.to_gpu(np.ones((3, 2), dtype=np.float32)))
        y = chainer.Variable(chainer.cuda.to_gpu(np.ones((3, 2), dtype=np.float32)))
        w = add2(x, y) 
        w.grad = chainer.cuda.to_gpu(np.ones((3, 2), dtype=np.float32))
        w.backward()

        f = lambda:(add2(x, y).data,)
        gx, gy = gradient_check.numerical_grad(f, (x.data, y.data), (w.grad,))
        gradient_check.assert_allclose(gx, x.grad)
        gradient_check.assert_allclose(gy, y.grad)


if __name__ == "__main__":
    unittest.main()

