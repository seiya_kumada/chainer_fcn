#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cPickle as pickle
from mini_batch_loader.mini_batch_loader import *
from VGGNet import VGGNet
from chainer import serializers
from myfcn3 import MyFcn3
from copy_model import *
from chainer import cuda, optimizers, Variable
import sys
import math
import time

#_/_/_/ paths _/_/_/ 
VGG_MODEL_PATH              = '../chainer-imagenet-vgg-master/VGG.model'
TRAINING_DATA_PATH          = "/home/ubuntu/data/voc_2012/Segmentation/augmented_training.txt"
TESTING_DATA_PATH           = "/home/ubuntu/data/voc_2012/Segmentation/testing.txt"
IMAGE_DIR_PATH              = "/home/ubuntu/data/voc_2012/JPEGImages_224"
SEGMENTATION_CLASS_DIR_PATH = "/home/ubuntu/data/voc_2012/SegmentationClass_224_Label"
PICKLE_DUMP_PATH            = "/home/ubuntu/results/voc2012/voc2012_myfcn3_{i}.pkl"

#_/_/_/ training parameters _/_/_/ 
LEARNING_RATE    = 0.001 
TRAIN_BATCH_SIZE = 10 
TEST_BATCH_SIZE  = 5 
EPOCHS           = 40 
DECAY_FACTOR     = 0.97 
SNAPSHOT_EPOCHS  = 20

def test(loader, model):
    sum_accuracy = 0
    sum_loss     = 0
    test_data_size = MiniBatchLoader.count_paths(TESTING_DATA_PATH)
    for i in range(0, test_data_size, TEST_BATCH_SIZE):
        raw_x, raw_t = loader.load_testing_data(np.array(range(i, i+TEST_BATCH_SIZE)))
        x = chainer.Variable(chainer.cuda.to_gpu(raw_x))
        t = chainer.Variable(chainer.cuda.to_gpu(raw_t))
        model.train = False
        model(x, t)
        sum_loss     += model.loss.data * TEST_BATCH_SIZE
        sum_accuracy += model.accuracy * TEST_BATCH_SIZE

    print("test mean loss {a}, accuracy {b}".format(a=sum_loss/test_data_size, b=sum_accuracy/test_data_size))
    sys.stdout.flush()


def main():
    print("#_/_/_/ load dataset _/_/_/")
    mini_batch_loader = MiniBatchLoader(
        TRAINING_DATA_PATH, 
        TESTING_DATA_PATH, 
        IMAGE_DIR_PATH, 
        SEGMENTATION_CLASS_DIR_PATH, 
        MyFcn3.IN_SIZE)

    #_/_/_/ load model _/_/_/

    print("# load VGG model")
    vgg = VGGNet()
    serializers.load_hdf5(VGG_MODEL_PATH, vgg)

    print("# load myfcn3 model")
    myfcn3 = MyFcn3()
    
    print("# copy W/b from vgg to myfcn3")
    copy_model(vgg, myfcn3)

    print("#_/_/_/ setup _/_/_/")

    myfcn3 = myfcn3.to_gpu()
    optimizer = chainer.optimizers.MomentumSGD(lr=LEARNING_RATE)
    optimizer.setup(myfcn3)

    print("#_/_/_/ training _/_/_/")

    train_data_size = MiniBatchLoader.count_paths(TRAINING_DATA_PATH)
    for epoch in range(1, EPOCHS+1):
        print("epoch %d" % epoch)
        sys.stdout.flush()
        indices = np.random.permutation(train_data_size)
        sum_accuracy = 0
        sum_loss     = 0

        for i in range(0, train_data_size, TRAIN_BATCH_SIZE):
            r = indices[i:i+TRAIN_BATCH_SIZE]
            raw_x, raw_y = mini_batch_loader.load_training_data(r)
            x = Variable(chainer.cuda.to_gpu(raw_x))
            y = Variable(chainer.cuda.to_gpu(raw_y))
            myfcn3.zerograds()
            myfcn3.train = True
            loss = myfcn3(x, y)
            loss.backward()
            optimizer.update()

            if math.isnan(loss.data):
                raise RuntimeError("ERROR in main: loss.data is nan!")

            sum_loss     += loss.data * TRAIN_BATCH_SIZE
            sum_accuracy += myfcn3.accuracy * TRAIN_BATCH_SIZE

        print("train mean loss {a}, accuracy {b}".format(a=sum_loss/train_data_size, b=sum_accuracy/train_data_size))
        sys.stdout.flush()
        test(mini_batch_loader, myfcn3)
        
        optimizer.lr *= DECAY_FACTOR 
        if epoch % SNAPSHOT_EPOCHS == 0:
            pickle.dump(myfcn3, open(PICKLE_DUMP_PATH.format(i=epoch), "wb"))

    print("#_/_/_/ testing _/_/_/")

    test(mini_batch_loader, myfcn3)
    
    print("#_/_/_/ saving _/_/_/")

    pickle.dump(myfcn3, open(PICKLE_DUMP_PATH.format(i="final"), "wb"))



if __name__ == '__main__':
    try:
        start = time.time()
        main()
        end = time.time()
        print("{s}[s]".format(s=end - start))
        print("{s}[m]".format(s=(end - start)/60))
        print("{s}[h]".format(s=(end - start)/60/60))
    except Exception as error:
        print(error)




