#!/usr/bin/evn python
# -*- coding: utf-8 -*-
import os
import cPickle
import numpy as np

# INPUT_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass_224_Label"
INPUT_DIR_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/cls_img_224_label"


def file_path_generator(dpath):
    for p in os.listdir(dpath):
        yield os.path.join(dpath, p)


def is_black_image(img):
    f = img == 0
    return np.all(f)


# voc2012
# 真っ黒画像は２枚だけ。
# 2008_001203.pkl
# 2008_003208.pkl

# sbd
# 30個が真っ黒。
INVALID_NAMES = {
    "2008_000148",
    "2008_000042",
    "2008_000695",
    "2008_001750",
    "2008_001823",
    "2008_002118",
    "2008_003208",
    "2008_003374",
    "2008_003829",
    "2008_004907",
    "2008_004923",
    "2008_005890",
    "2008_006249",
    "2008_006522",
    "2008_006600",
    "2008_007197",
    "2009_000102",
    "2009_001000",
    "2009_002626",
    "2009_003384",
    "2010_000956",
    "2010_000979",
    "2010_001310",
    "2010_003549",
    "2010_005054",
    "2010_005164",
    "2011_000249",
    "2011_001476",
    "2011_002325",
    "2011_002897",
}

SRC_TRAIN_TXT_FILE_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/train.txt"
DST_TRAIN_TXT_FILE_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/train_without_black.txt"

SRC_VAL_TXT_FILE_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/val.txt"
DST_VAL_TXT_FILE_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/val_without_black.txt"


def find_invalid_files(dpath):
    for fp in file_path_generator(dpath):
        label = cPickle.load(open(fp))
        if is_black_image(label):
            print(os.path.basename(fp))


def name_generator(path):
    for line in open(path):
        line = line.strip()
        yield line


def remove_invalid_files(src_path, dst_path):
    fout = open(dst_path, "w")
    for name in name_generator(src_path):
        if name not in INVALID_NAMES:
            fout.write(name + "\n")


if __name__ == "__main__":
    # find_invalid_files(INPUT_DIR_PATH)
    remove_invalid_files(SRC_VAL_TXT_FILE_PATH, DST_VAL_TXT_FILE_PATH)


