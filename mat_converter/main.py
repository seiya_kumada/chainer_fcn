#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import scipy.io.matlab.mio
import os
from voc_label_colormap import *
import cv2

MAT_DIR_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/cls"
OUTPUT_DIR_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/cls_img"
CLASSES = 21


def count(img):
    for k in range(21):
        r = img == k
        yield (k, np.count_nonzero(r))


def file_path_generator(p):
    for f in os.listdir(p):
        yield os.path.join(p, f)


def extract(p):
    data = scipy.io.loadmat(p)
    gt_cls = data["GTcls"]
    return gt_cls["Segmentation"][0, 0]


def test(p):
    data = scipy.io.loadmat(p)
    gt_cls = data["GTcls"]
    # boundaries = gt_cls["Boundaries"]
    segmentation = gt_cls["Segmentation"]
    # categories_present = gt_cls["CategoriesPresent"]

    # print("boundaries.shape:         {n}".format(n=boundaries.shape))
    # print("segmentation.shape:       {n}".format(n=segmentation.shape))
    # print("categories_present.shape: {n}".format(n=categories_present.shape))

    # print("boundaries[0, 0].shape:         {n}".format(n=boundaries[0, 0].shape))
    print("segmentation[0, 0].shape:       {n}".format(n=segmentation[0, 0].shape))
    # print("categories_present[0, 0].shape: {n}".format(n=categories_present[0, 0].shape))
    # print(categories_present[0, 0][0])
    # print(boundaries[0, 0][0])
    count(segmentation[0, 0])


def convert_to_image(l, cm):
    (r, c) = l.shape
    img = np.ones((r, c, 3))
    for k in range(CLASSES):
        img[l == k] = cm[k]
    return img


if __name__ == "__main__":
    color_map = make_color_map()

    for i, path in enumerate(file_path_generator(MAT_DIR_PATH)):
        name, _ = os.path.splitext(os.path.basename(path))
        labels = extract(path)
        image = convert_to_image(labels, color_map)
        out_path = os.path.join(OUTPUT_DIR_PATH, name + ".png")
        cv2.imwrite(out_path, image)