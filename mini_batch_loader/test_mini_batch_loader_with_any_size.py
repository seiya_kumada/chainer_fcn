#!/usr/bin/env python2.7
# coding:utf-8

import unittest
from mini_batch_loader_with_any_size import *

TRAINING_DATA_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/test_train.txt"
TESTING_DATA_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/test_val.txt"
IMAGE_DIR_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/img"
SEGMENTATION_CLASS_DIR_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/cls"
TEST_DATA_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/unit_test.txt"

VOC_TRAINING_DATA_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/test_training.txt"
VOC_TESTING_DATA_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/test_testing.txt"
VOC_IMAGE_DIR_PATH = "/Users/kumada/Data/VOC2012/JPEGImages"
VOC_SEGMENTATION_CLASS_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass"
VOC_TEST_DATA_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/unit_test.txt"


class MyTestCase(unittest.TestCase):

    def test_path_label_generator(self):
        generator = MiniBatchLoaderWithAnySize.path_label_generator(TRAINING_DATA_PATH, IMAGE_DIR_PATH,
                                                                    SEGMENTATION_CLASS_DIR_PATH, MAT_EXT)
        self.assertEqual(len(list(generator)), 10)

        generator = MiniBatchLoaderWithAnySize.path_label_generator(TESTING_DATA_PATH, IMAGE_DIR_PATH,
                                                                    SEGMENTATION_CLASS_DIR_PATH, MAT_EXT)
        self.assertEqual(len(list(generator)), 10)

    def test_read_paths(self):
        training_path_infos = MiniBatchLoaderWithAnySize.read_paths(TRAINING_DATA_PATH, IMAGE_DIR_PATH,
                                                                    SEGMENTATION_CLASS_DIR_PATH, MAT_EXT)

        test = training_path_infos[0]
        self.assertEqual(test.image_path, os.path.join(IMAGE_DIR_PATH, "2008_000002.jpg"))
        self.assertEqual(test.label_path, os.path.join(SEGMENTATION_CLASS_DIR_PATH, "2008_000002.mat"))

        test = training_path_infos[-1]
        self.assertEqual(test.image_path, os.path.join(IMAGE_DIR_PATH, "2008_000033.jpg"))
        self.assertEqual(test.label_path, os.path.join(SEGMENTATION_CLASS_DIR_PATH, "2008_000033.mat"))

    def test_count_paths(self):
        c = MiniBatchLoaderWithAnySize.count_paths(TRAINING_DATA_PATH)
        self.assertEqual(c, 10)
        c = MiniBatchLoaderWithAnySize.count_paths(TESTING_DATA_PATH)
        self.assertEqual(c, 10)

    def test_load_data(self):

        test_path_infos = MiniBatchLoaderWithAnySize.read_paths(TEST_DATA_PATH, IMAGE_DIR_PATH,
                                                                SEGMENTATION_CLASS_DIR_PATH, MAT_EXT)
        loader = MiniBatchLoaderWithAnySize(
            TRAINING_DATA_PATH,
            TESTING_DATA_PATH,
            IMAGE_DIR_PATH,
            SEGMENTATION_CLASS_DIR_PATH)

        (xs, ys) = loader.load_data(test_path_infos, 0, 255.0)
        row = 375
        col = 500
        self.assertTrue(xs.dtype == np.float32, "")
        self.assertTrue(ys.dtype == np.int32, "")

        self.assertEqual(xs.shape, (1, 3, row, col))
        self.assertEqual(ys.shape, (1, row, col))

        self.assertEqual(20, ys[0,  93, 144])
        self.assertEqual(0, ys[0, 364, 483])

        a = np.array([20, 23, 27])  # (b,g,r)
        b = (a - loader.mean)/255
        c = xs[0, :, 359, 481]
        epsilon = 1.0e-5
        self.assertAlmostEqual(b[0], c[0], delta=epsilon)
        self.assertAlmostEqual(b[1], c[1], delta=epsilon)
        self.assertAlmostEqual(b[2], c[2], delta=epsilon)

    def test_load_voc_data(self):
        test_path_infos = MiniBatchLoaderWithAnySize.read_paths(VOC_TEST_DATA_PATH, VOC_IMAGE_DIR_PATH,
                                                                VOC_SEGMENTATION_CLASS_DIR_PATH, PNG_EXT)
        loader = MiniBatchLoaderWithAnySize(
            VOC_TRAINING_DATA_PATH,
            VOC_TESTING_DATA_PATH,
            VOC_IMAGE_DIR_PATH,
            VOC_SEGMENTATION_CLASS_DIR_PATH, MiniBatchLoaderWithAnySize.VOC)

        (xs, ys) = loader.load_data(test_path_infos, 0, 255.0)

        row = 375
        col = 500
        self.assertTrue(xs.dtype == np.float32, "")
        self.assertTrue(ys.dtype == np.int32, "")

        self.assertEqual(xs.shape, (1, 3, row, col))
        self.assertEqual(ys.shape, (1, row, col))

        self.assertEqual(13, ys[0, 252, 283])
        self.assertEqual(0, ys[0, 17, 20])

        a = np.array([87, 113, 137])  # (b,g,r)
        b = (a - loader.mean) / 255
        c = xs[0, :, 344, 448]
        epsilon = 1.0e-5
        self.assertAlmostEqual(b[0], c[0], delta=epsilon)
        self.assertAlmostEqual(b[1], c[1], delta=epsilon)
        self.assertAlmostEqual(b[2], c[2], delta=epsilon)

    def test_load_testing_data(self):

        loader = MiniBatchLoaderWithAnySize(
            TRAINING_DATA_PATH,
            TESTING_DATA_PATH,
            IMAGE_DIR_PATH,
            SEGMENTATION_CLASS_DIR_PATH)

        (xs, ys) = loader.load_testing_data(0)
        row = 333
        col = 500
        self.assertTrue(xs.dtype == np.float32, "")
        self.assertTrue(ys.dtype == np.int32, "")

        self.assertEqual(xs.shape, (1, 3, row, col))
        self.assertEqual(ys.shape, (1, row, col))

        self.assertEqual(19, ys[0, 32, 474])

        a = np.array([16, 13, 9])  # (b,g,r)
        b = (a - loader.mean) / 255
        c = xs[0, :, 304, 460]
        epsilon = 1.0e-5
        self.assertAlmostEqual(b[0], c[0], delta=epsilon)
        self.assertAlmostEqual(b[1], c[1], delta=epsilon)
        self.assertAlmostEqual(b[2], c[2], delta=epsilon)

    def test_load_voc_testing_data(self):
        loader = MiniBatchLoaderWithAnySize(
            VOC_TRAINING_DATA_PATH,
            VOC_TESTING_DATA_PATH,
            VOC_IMAGE_DIR_PATH,
            VOC_SEGMENTATION_CLASS_DIR_PATH,
            MiniBatchLoaderWithAnySize.VOC)

        (xs, ys) = loader.load_testing_data(0)
        row = 375
        col = 500
        self.assertTrue(xs.dtype == np.float32, "")
        self.assertTrue(ys.dtype == np.int32, "")

        self.assertEqual(xs.shape, (1, 3, row, col))
        self.assertEqual(ys.shape, (1, row, col))

        self.assertEqual(13, ys[0, 252, 283])
        self.assertEqual(0, ys[0, 17, 20])
        self.assertEqual(-1, ys[0, 184, 388])

        a = np.array([87, 113, 137])  # (b,g,r)
        b = (a - loader.mean) / 255
        c = xs[0, :, 344, 448]
        epsilon = 1.0e-5
        self.assertAlmostEqual(b[0], c[0], delta=epsilon)
        self.assertAlmostEqual(b[1], c[1], delta=epsilon)
        self.assertAlmostEqual(b[2], c[2], delta=epsilon)

    @staticmethod
    def check(ys):
        s = set()
        for v in ys.flatten():
            s.add(v)
        print(s)

    def test_load_voc_training_data(self):
        loader = MiniBatchLoaderWithAnySize(
            VOC_TRAINING_DATA_PATH,
            VOC_TESTING_DATA_PATH,
            VOC_IMAGE_DIR_PATH,
            VOC_SEGMENTATION_CLASS_DIR_PATH,
            MiniBatchLoaderWithAnySize.VOC)

        (xs, ys) = loader.load_training_data(0)
        row = 375
        col = 500
        self.assertTrue(xs.dtype == np.float32, "")
        self.assertTrue(ys.dtype == np.int32, "")
        MyTestCase.check(ys)
        self.assertEqual(xs.shape, (1, 3, row, col))
        self.assertEqual(ys.shape, (1, row, col))

        self.assertEqual(12, ys[0, 195, 134])
        self.assertEqual(15, ys[0, 105, 58])
        self.assertEqual(-1, ys[0, 138, 472])

        a = np.array([122, 122, 116])  # (b,g,r)
        b = (a - loader.mean) / 255
        c = xs[0, :, 350, 24]
        epsilon = 1.0e-5
        self.assertAlmostEqual(b[0], c[0], delta=epsilon)
        self.assertAlmostEqual(b[1], c[1], delta=epsilon)
        self.assertAlmostEqual(b[2], c[2], delta=epsilon)


if __name__ == '__main__':
    unittest.main()
