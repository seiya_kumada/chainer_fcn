#!/usr/bin/env python2.7
# coding:utf-8

import collections
import os
import numpy as np
import cv2
import scipy.io
from PIL import Image

PathInfo = collections.namedtuple('PathInfo', ['image_path', 'label_path'])
IMAGE_EXT = ".jpg"
MAT_EXT = ".mat"
PNG_EXT = ".png"


class MiniBatchLoaderWithAnySize(object):

    BSD = "bsd"
    VOC = "voc"

    def __init__(self, train_path, test_path, image_dir_path, seg_class_dir_path, kind=BSD):

        if kind == MiniBatchLoaderWithAnySize.BSD:
            self.load_label = MiniBatchLoaderWithAnySize.load_bsd_label
            extension = MAT_EXT
        else:
            self.load_label = MiniBatchLoaderWithAnySize.load_voc_label
            extension = PNG_EXT

        # load data paths
        self.training_path_infos = self.read_paths(train_path, image_dir_path, seg_class_dir_path, extension)
        self.testing_path_infos = self.read_paths(test_path, image_dir_path, seg_class_dir_path, extension)

        # load a mean image
        self.mean = np.array([104.00699, 116.66877, 122.67892])

    # test ok
    @staticmethod
    def path_label_generator(txt_path, src_path, seg_path, extension):
        for i, line in enumerate(open(txt_path)):
            line = line.strip()
            src_full_path = os.path.join(src_path, line + IMAGE_EXT)
            seg_full_path = os.path.join(seg_path, line + extension)
            if os.path.isfile(src_full_path) and os.path.isfile(seg_full_path):
                yield src_full_path, seg_full_path
            else:
                print(src_full_path, seg_full_path)
            if i % 1000 == 0:
                print("now generating paths...{n}".format(n=i))

    # test ok
    @staticmethod
    def count_paths(path):
        c = 0
        for _ in open(path):
            c += 1
        return c

    # test ok
    @staticmethod
    def read_paths(txt_path, src_path, seg_path, extension):
        cs = []
        for pair in MiniBatchLoaderWithAnySize.path_label_generator(txt_path, src_path, seg_path, extension):
            cs.append(PathInfo(pair[0], pair[1]))
        return cs

    # test ok
    def load_training_data(self, index, factor=255.0):
        return self.load_data(self.training_path_infos, index, factor)

    # test ok
    def load_testing_data(self, index, factor=255.0):
        return self.load_data(self.testing_path_infos, index, factor)

    # test ok
    @staticmethod
    def convert_to_minus_one(mat):
        mask = mat == 255
        mat[mask] = -1
        return mat

    @staticmethod
    def load_bsd_label(path):
        mat = scipy.io.loadmat(path)
        label = mat['GTcls'][0]['Segmentation'][0].astype(np.int32)
        label = label[np.newaxis, :]
        return label

    # ok
    @staticmethod
    def load_voc_label(path):
        im = Image.open(path)
        label = np.array(im, dtype=np.int32)
        label = label[np.newaxis, ...]
        return MiniBatchLoaderWithAnySize.convert_to_minus_one(label)

    # test ok
    def load_data(self, path_infos, index, factor):
        path, label_path = path_infos[index]
        img = cv2.imread(path)
        if img is None:
            raise RuntimeError("invalid image: {i}".format(i=path))
        img = ((img - self.mean) / factor).transpose(2, 0, 1).astype(np.float32)
        img = img[np.newaxis, :]

        label = self.load_label(label_path)
        return img, label
