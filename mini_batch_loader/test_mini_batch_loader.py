#!/usr/bin/env python2.7
# coding:utf-8

import unittest
from mini_batch_loader import *

TRAINING_DATA_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/train.txt"
TRAINING_DATA_PATH_2 = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/training.txt"
TESTING_DATA_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/val.txt"
TESTING_DATA_PATH_2 = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/testing.txt"
IMAGE_DIR_PATH = "/Users/kumada/Data/VOC2012/JPEGImages_224"
SEGMENTATION_CLASS_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass_224_Label"
TEST_DATA_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/unit_test_train.txt"

IMAGE_WITH_ANY_SIZE_DIR_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/img"
SEGMENTATION_CLASS_WITH_ANY_SIZE_DIR_PATH = "/Users/kumada/Data/SBD/benchmark_RELEASE/dataset/cls"
TEST_DATA_WITH_ANY_SIZE_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/unit_test_train_with_any_size.txt"


class MyTestCase(unittest.TestCase):

    def test_path_label_generator(self):
        generator = MiniBatchLoader.path_label_generator(TRAINING_DATA_PATH, IMAGE_DIR_PATH,
                                                         SEGMENTATION_CLASS_DIR_PATH)
        self.assertEqual(len(list(generator)), 1464)

        generator = MiniBatchLoader.path_label_generator(TESTING_DATA_PATH, IMAGE_DIR_PATH,
                                                         SEGMENTATION_CLASS_DIR_PATH)
        self.assertEqual(len(list(generator)), 1449)

    def test_read_paths(self):
        training_path_infos = MiniBatchLoader.read_paths(TRAINING_DATA_PATH, IMAGE_DIR_PATH,
                                                         SEGMENTATION_CLASS_DIR_PATH)

        test = training_path_infos[0]
        self.assertEqual(test.image_path, os.path.join(IMAGE_DIR_PATH, "2007_000032.png"))
        self.assertEqual(test.label_path, os.path.join(SEGMENTATION_CLASS_DIR_PATH, "2007_000032.pkl"))

        test = training_path_infos[-1]
        self.assertEqual(test.image_path, os.path.join(IMAGE_DIR_PATH, "2011_003255.png"))
        self.assertEqual(test.label_path, os.path.join(SEGMENTATION_CLASS_DIR_PATH, "2011_003255.pkl"))

    def test_count_paths(self):
        c = MiniBatchLoader.count_paths(TRAINING_DATA_PATH)
        self.assertEqual(c, 1464)
        c = MiniBatchLoader.count_paths(TESTING_DATA_PATH)
        self.assertEqual(c, 1449)

    def test_load_data(self):

        test_path_infos = MiniBatchLoader.read_paths(TEST_DATA_PATH, IMAGE_DIR_PATH, SEGMENTATION_CLASS_DIR_PATH)
        in_size = 224
        loader = MiniBatchLoader(
            TRAINING_DATA_PATH,
            TESTING_DATA_PATH,
            IMAGE_DIR_PATH,
            SEGMENTATION_CLASS_DIR_PATH,
            in_size)

        (xs, ys) = loader.load_data(test_path_infos, [0])
        self.assertEqual(xs.shape, (1, 3, in_size, in_size))
        self.assertEqual(ys.shape, (1, in_size, in_size))

        self.assertEqual(15, ys[0, 143, 36])
        self.assertEqual(15, ys[0, 164, 29])
        self.assertEqual(15, ys[0, 208, 163])

        self.assertEqual(2, ys[0, 200, 117])
        self.assertEqual(2, ys[0, 204, 210])
        self.assertEqual(2, ys[0, 119, 35])

        a = np.array([234, 250, 253])  # (b,g,r)
        b = (a - loader.mean)/255
        c = xs[0, :, 112, 88]
        epsilon = 1.0e-5
        self.assertAlmostEqual(b[0], c[0], delta=epsilon)
        self.assertAlmostEqual(b[1], c[1], delta=epsilon)
        self.assertAlmostEqual(b[2], c[2], delta=epsilon)

if __name__ == '__main__':
    unittest.main()
