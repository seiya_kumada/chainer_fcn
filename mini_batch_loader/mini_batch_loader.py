#!/usr/bin/env python2.7
# coding:utf-8

import collections
import os
import numpy as np
import cv2
import cPickle

PathInfo = collections.namedtuple('PathInfo', ['image_path', 'label_path'])


class MiniBatchLoader(object):
    LABEL_COUNT = 21

    def __init__(self, train_path, test_path, image_dir_path, seg_class_dir_path, in_size):

        # load data paths
        self.training_path_infos = self.read_paths(train_path, image_dir_path, seg_class_dir_path)
        self.testing_path_infos = self.read_paths(test_path, image_dir_path, seg_class_dir_path)

        # load a mean image
        self.mean = np.array([103.939, 116.779, 123.68])
        self.in_size = in_size

    # test ok
    @staticmethod
    def path_label_generator(txt_path, src_path, seg_path):
        for line in open(txt_path):
            line = line.strip()
            src_full_path = os.path.join(src_path, line + ".png")
            seg_full_path = os.path.join(seg_path, line + ".pkl")
            if os.path.isfile(src_full_path) and os.path.isfile(seg_full_path):
                yield src_full_path, seg_full_path
            else:
                print(seg_full_path)

    # test ok
    @staticmethod
    def count_paths(path):
        c = 0
        for _ in open(path):
            c += 1
        return c

    # test ok
    @staticmethod
    def read_paths(txt_path, src_path, seg_path):
        cs = []
        for pair in MiniBatchLoader.path_label_generator(txt_path, src_path, seg_path):
            cs.append(PathInfo(pair[0], pair[1]))
        return cs

    def load_training_data(self, indices):
        return self.load_data(self.training_path_infos, indices)

    def load_testing_data(self, indices):
        return self.load_data(self.testing_path_infos, indices)

    # test ok
    def load_data(self, path_infos, indices):
        mini_batch_size = len(indices)
        in_channels = 3
        xs = np.zeros((mini_batch_size, in_channels,  self.in_size, self.in_size)).astype(np.float32)
        ys = np.zeros((mini_batch_size, self.in_size, self.in_size)).astype(np.int32)

        for i, index in enumerate(indices):
            path, label = path_infos[index]
            
            img = cv2.imread(path)
            if img is None:
                raise RuntimeError("invalid image: {i}".format(i=path))
            xs[i, :, :, :] = ((img - self.mean)/255).transpose(2, 0, 1)

            img = cPickle.load(open(label))
            if img is None:
                raise RuntimeError("invalid image: {i}".format(i=label))
            ys[i, :, :] = img
        return xs, ys
