#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import cPickle
import numpy as np

LABEL_DIR_PATH = "/Volumes/Untitled/mac/Data/toshiba-ext/SBD/benchmark_RELEASE/dataset/cls_img_224_label"


def file_generator(path):
    for p in os.listdir(path):
        yield os.path.join(path, p)


def count(img):
    for k in range(21):
        yield (k, np.count_nonzero(img == k))


if __name__ == "__main__":

    for i, fp in enumerate(file_generator(LABEL_DIR_PATH)):
        image = cPickle.load(open(fp))

        (row, col) = image.shape
        total = row * col

        d = dict(count(image))
        diff = total - sum(d.values())

        e = np.count_nonzero(image == -1)

        assert(e == diff)
        ratio = diff/float(total)*100
        print("{n}: {d}, -1: {e}(=={f}), {p}%".format(n=os.path.basename(fp), d=d, e=e, f=diff, p=ratio))
        if i == 20:
            break
