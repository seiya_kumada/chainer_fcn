import unittest
import main
import os
import cPickle
import numpy as np

SRC_SEGMENTATION_CLASS_DIR_PATH = "/Users/kumada/Data/VOC2012/in_test"
DST_SEGMENTATION_CLASS_DIR_PATH = "/Users/kumada/Data/VOC2012/out_test"
FILE_NAME = "2007_000032.pkl"
SRC_TRAIN_TXT_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/unit_test_train.txt"
DST_TRAIN_TXT_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/augmented_unit_test_train.txt"


class MyTestCase(unittest.TestCase):
    def test_flip_label(self):
        main.flip_label(SRC_SEGMENTATION_CLASS_DIR_PATH, DST_SEGMENTATION_CLASS_DIR_PATH, ".pkl")
        in_path = os.path.join(SRC_SEGMENTATION_CLASS_DIR_PATH, FILE_NAME)
        out_path = os.path.join(DST_SEGMENTATION_CLASS_DIR_PATH, FILE_NAME)
        simg = cPickle.load(open(in_path))
        dimg = cPickle.load(open(out_path))
        col = simg.shape[1]

        for j in range(col):
            r = simg[:, col - 1 - j] == dimg[:, j]
            self.assertEqual(np.all(r), True)

    def test_augment_names(self):
        main.augment_names(SRC_TRAIN_TXT_PATH, DST_TRAIN_TXT_PATH)


if __name__ == '__main__':
    unittest.main()
