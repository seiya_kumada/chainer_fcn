#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import cv2
import cPickle
import random

SRC_IMAGE_DIR_PATH = "/Users/kumada/Data/VOC2012/JPEGImages_448"
DST_IMAGE_DIR_PATH = "/Users/kumada/Data/VOC2012/JPEGImages_448_flipped"

SRC_SEGMENTATION_CLASS_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass_448_Label"
DST_SEGMENTATION_CLASS_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass_448_Label_flipped"

# SRC_TRAIN_TXT_PATH = "/Users/seiya_kumada/Downloads/training.txt"
# DST_TRAIN_TXT_PATH = "/Users/seiya_kumada/Downloads/augmented_training.txt"


def file_path_generator(in_path, out_path, ext):
    for p in os.listdir(in_path):
        f, _ = os.path.splitext(p)
        yield os.path.join(in_path, p), os.path.join(out_path, f + "_flipped." + ext)


def flip_image(in_path, out_path, ext):
    for in_fp, out_fp in file_path_generator(in_path, out_path, ext):
        image = cv2.imread(in_fp)
        if image is None:
            raise RuntimeError("error")
        image = cv2.flip(image, 1)
        cv2.imwrite(out_fp, image)


def flip_label(in_path, out_path, ext):
    for in_fp, out_fp in file_path_generator(in_path, out_path, ext):
        image = cPickle.load(open(in_fp))
        if image is None:
            raise RuntimeError("error")
        image = cv2.flip(image, 1)
        cPickle.dump(image, open(out_fp, "wb"))


def augment_names(src_path, dst_path):
    results = []
    for name in open(src_path):
        name = name.strip()
        results.append(name)
        results.append(name + "_flipped")
    random.shuffle(results)
    with open(dst_path, "w") as ofs:
        for f in results:
            ofs.write(f + "\n")

if __name__ == "__main__":
    flip_image(SRC_IMAGE_DIR_PATH, DST_IMAGE_DIR_PATH, "png")
    flip_label(SRC_SEGMENTATION_CLASS_DIR_PATH, DST_SEGMENTATION_CLASS_DIR_PATH, "pkl")
    # augment_names(SRC_TRAIN_TXT_PATH, DST_TRAIN_TXT_PATH)
