#!/usr/bin/env python
# coding:utf-8

import unittest
from myfcn2 import MyFcn2 
import chainer
import numpy as np
from chainer import gradient_check
import chainer.functions as F
import cv2
import os

class TestMyFcn2(unittest.TestCase):
    def test_init(self):
        myfcn = MyFcn2().to_gpu() 
        self.assertNotEqual(myfcn, None)

    def test_call(self):
        batch_size = 5 
        channel = 3
        row = 224
        col = 224
        x = chainer.Variable(chainer.cuda.to_gpu(np.ones((batch_size, channel, row, col)).astype(np.float32)))
        t = chainer.Variable(chainer.cuda.to_gpu(np.ones((batch_size, row, col)).astype(np.int32)))
        myfcn = MyFcn2().to_gpu()
        myfcn.train = True 
        r = myfcn(x, t)
        #print(r.data)
        self.assertEqual(myfcn.p3_shape, (batch_size, MyFcn2.CLASSES, row/ 8, col/ 8))
        self.assertEqual(myfcn.p4_shape, (batch_size, MyFcn2.CLASSES, row/16, col/16))
        self.assertEqual(myfcn.u4_shape, (batch_size, MyFcn2.CLASSES, row/ 8, col/ 8))
        self.assertEqual(myfcn.u8_shape, (batch_size, MyFcn2.CLASSES, row/ 8, col/ 8))
        self.assertEqual(myfcn.final_shape, (batch_size, MyFcn2.CLASSES, row, col))

    def make_labels(self, bs):
        l = np.array([
            [0, 1],
            [0, 1],
            [0, 1],
            ])
        r = np.zeros((bs, l.shape[0], l.shape[1])).astype(np.int32)
        r[:] = l
        return r 

    def make_h(self, bs):
        r0 = np.zeros((3, 2))
        r0[0,0] = 0.1
        r0[1,0] = 0.1
        r0[2,0] = 0.1
        #print(r0) 
        r1 = np.zeros((3, 2))
        r1[0,1] = 0.1
        r1[1,1] = 0.1
        r1[2,1] = 0.1
        #print(r1)
        classes = 2
        y = np.zeros((bs, classes, r0.shape[0], r0.shape[1])).astype(np.float32)
        y[:,0] = r0
        y[:,1] = r1
        return y

    def test_softmax_cross_entropy(self):
        batch_size = 4 
        labels = self.make_labels(batch_size)
        sh = self.make_h(batch_size)
        #print(h)
        h = chainer.Variable(chainer.cuda.to_gpu(sh))
        t = chainer.Variable(chainer.cuda.to_gpu(labels))
        r = F.softmax_cross_entropy(h, t)
        #print(r.data)
        self.assertAlmostEqual(0.644396662712, r.data) 
        h = chainer.Variable(chainer.cuda.to_gpu(np.argmax(sh, axis=1)))
        self.assertEqual((4, 3, 2), h.data.shape)
        self.assertEqual((4, 3, 2), t.data.shape)
        
        s = (h.data == t.data).mean(axis=1).mean(axis=1)
        self.assertEqual(s[0], 1) 
        self.assertEqual(s[1], 1) 
        self.assertEqual(s[2], 1) 
        self.assertEqual(s[3], 1) 

    def test_softmax_cross_entropy_3(self):
        batch_size = 4 
        h = self.make_h(batch_size)
        l = self.make_labels(batch_size)
        h = chainer.Variable(chainer.cuda.to_gpu(h))
        l = chainer.Variable(chainer.cuda.to_gpu(l))
        
        ch = chainer.cuda.cupy.argmax(h.data, axis=1)
        s = (ch == l.data).mean(axis=1).mean(axis=1).mean()
        self.assertEqual(1, s) 

    def test_softmax_cross_entropy_4(self):
        batch_size = 4 
        raw_h = self.make_h(batch_size)
        raw_l = self.make_labels(batch_size)
        raw_l[:,0,0] = -1
        gpu_h = chainer.Variable(chainer.cuda.to_gpu(raw_h))
        gpu_l = chainer.Variable(chainer.cuda.to_gpu(raw_l))

        cpu_h = chainer.cuda.to_cpu(gpu_h.data)
        cpu_l = chainer.cuda.to_cpu(gpu_l.data)
        mask = cpu_l != -1 
        reduced_cpu_h = np.argmax(cpu_h, axis=1)
        masked_reduced_cpu_h = reduced_cpu_h[mask]
        masked_cpu_l = cpu_l[mask]
        s = (masked_reduced_cpu_h == masked_cpu_l).mean()
        self.assertEqual(1, s) 

    def test_upsample_bilinearly(self):
        dpath = "/home/ubuntu/data/sbd/unit_test/inputs/"
        batch_size = 3 
        in_channels = 3
        out_channels = 3
        rows = 224
        cols = 224
        x = np.zeros((batch_size, in_channels, rows, cols)).astype(np.float32)
        for i, p in enumerate(os.listdir(dpath)):
            fp = os.path.join(dpath, p)
            image = cv2.imread(fp).transpose(2, 0, 1).astype(np.float32)
            x[i] = image

        vx = chainer.Variable(chainer.cuda.to_gpu(x))
        
        stride = 2
        ksize = 4 
        pad = 1
       
        w = MyFcn2.make_bilinear_interpolation_kernel(in_channels, out_channels, ksize=ksize, stride=stride, pad=pad)
        vw = chainer.Variable(chainer.cuda.to_gpu(w))
        o = F.deconvolution_2d(vx, vw, stride=stride, pad=pad)
        
        odpath = "/home/ubuntu/data/sbd/unit_test/outputs"
        for i, img in enumerate(o.data):
            oi = chainer.cuda.to_cpu(img.transpose(1, 2, 0).astype(np.uint8))
            ofp = os.path.join(odpath, "hoge_" + str(i) + ".png")
            cv2.imwrite(ofp, oi)

    def test_copy_weights(self):
        a = chainer.links.Deconvolution2D(3, 3, ksize=4, stride=2, pad=1)
        ws = np.ones((3, 3, 4, 4)).astype(np.float32)
        MyFcn2.copy_weights(a, ws)
        ps = dict(a.namedparams())
        w = ps["/W"]
        self.assertEqual(np.all(w.data == ws), True)

if __name__ == "__main__":
    unittest.main()

