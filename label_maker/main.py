#!/usr/bin/env python2.7
# -*- coding:utf-8 -*-

import os
from voc_label_colormap import *
import cv2
import collections
import cPickle

SRC_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass_448"
DST_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass_448_Label"

# SRC_DIR_PATH = "/Volumes/Untitled/mac/Data/toshiba-ext/SBD/benchmark_RELEASE/dataset/test_png"
# DST_DIR_PATH = "/Volumes/Untitled/mac/Data/toshiba-ext/SBD/benchmark_RELEASE/dataset/test_pkl"


# test ok
def file_generator(sdpath, ddpath):
    for f in os.listdir(sdpath):
        ip = os.path.join(sdpath, f)
        (name, _) = os.path.splitext(f)
        op = os.path.join(ddpath, name + ".pkl")
        yield (ip, op)


# test ok
def make_label_image(icm, img):
    src = np.reshape(img, (-1, 3))
    r = np.ones((src.shape[0])).astype(np.int32)
    for (i, s) in enumerate(src):
        r[i] = icm[tuple(s)]
    r = np.reshape(r, (img.shape[0], img.shape[1]))
    return r


# test ok
def make_inverse_color_map(cm):
    icm = collections.defaultdict(lambda: -1)
    for (i, t) in enumerate(map(tuple, cm)):
        icm[t] = i
    return icm

if __name__ == "__main__":
    color_map = make_color_map()
    inverse_color_map = make_inverse_color_map(color_map)

    for (input_path, output_path) in file_generator(SRC_DIR_PATH, DST_DIR_PATH):
        input_img = cv2.imread(input_path)
        dst = make_label_image(inverse_color_map, input_img)
        cPickle.dump(dst, open(output_path, "wb"))
