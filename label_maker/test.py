import unittest
from main import *
import cPickle

PICKLE_PATH = "/Users/kumada/Data/VOC2012/unit_test/pickle.pkl"


class MyTestCase(unittest.TestCase):
    def test_make_inverse_color_map(self):
        cm = make_color_map()
        icm = make_inverse_color_map(cm)
        self.assertEqual(icm[(1, 1, 1)], -1)
        self.assertEqual(icm[(0, 0, 0)], 0)
        self.assertEqual(icm[(0, 0, 128)], 1)
        self.assertEqual(icm[(128, 64, 0)], 20)

    def test_make_label_image(self):
        img = np.array([[0, 0, 0], [0, 0, 128], [0, 128, 0], [0, 0, 64]])
        img = np.reshape(img, (2, 2, 3))
        cm = make_color_map()
        icm = make_inverse_color_map(cm)
        d = make_label_image(icm, img)
        self.assertEqual(d[0, 0], 0)
        self.assertEqual(d[0, 1], 1)
        self.assertEqual(d[1, 0], 2)
        self.assertEqual(d[1, 1], 8)
        self.assertEqual((2, 2), d.shape)

    def test_pickle_dump(self):
        img = np.array([[0, 1, 0], [0, 0, 128], [0, 128, 0], [0, 0, 64]])
        img = np.reshape(img, (2, 2, 3))
        cm = make_color_map()
        icm = make_inverse_color_map(cm)
        d = make_label_image(icm, img)
        self.assertEqual(d[0, 0], -1)
        self.assertEqual(d[0, 1], 1)
        self.assertEqual(d[1, 0], 2)
        self.assertEqual(d[1, 1], 8)
        self.assertEqual((2, 2), d.shape)

        cPickle.dump(d, open(PICKLE_PATH, "wb"))

    def test_pickle_load(self):
        d = cPickle.load(open(PICKLE_PATH))
        self.assertEqual(d[0, 0], -1)
        self.assertEqual(d[0, 1], 1)
        self.assertEqual(d[1, 0], 2)
        self.assertEqual(d[1, 1], 8)
        self.assertEqual((2, 2), d.shape)

    def test_file_generator(self):
        for (ip, op) in file_generator(SRC_DIR_PATH, DST_DIR_PATH):
            src_name, src_ext = os.path.splitext(ip)
            dst_name, dst_ext = os.path.splitext(op)
            self.assertEqual(src_ext, ".png")
            self.assertEqual(dst_ext, ".pkl")
            self.assertEqual(os.path.basename(src_name), os.path.basename(dst_name))


if __name__ == '__main__':
    unittest.main()
