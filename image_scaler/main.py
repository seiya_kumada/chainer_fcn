#!/usr/local/env python2.7
# -*- coding: utf-8 -*-

import os
from image_cropper import *

SEG_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass"
DST_SEG_DIR_PATH = "/Users/kumada/Data/VOC2012/SegmentationClass_448"

IMG_DIR_PATH = "/Users/kumada/Data/VOC2012/JPEGImages"
DST_IMG_DIR_PATH = "/Users/kumada/Data/VOC2012/JPEGImages_448"

TRAINING_FILE_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/train.txt"
TESTING_FILE_PATH = "/Users/kumada/Data/VOC2012/ImageSets/Segmentation/val.txt"

DST_SIZE = 448


def check_size(dpath):
    for path in os.listdir(dpath):
        fp = os.path.join(dpath, path)
        img = cv2.imread(fp)
        if img is not None:
            if img.shape[0] != DST_SIZE or img.shape[1] != DST_SIZE:
                print(path, img.shape)


def resize_images(path):
    cropper = ImageCropper(DST_SIZE)

    for name in open(path):
        # get a name
        name = name.strip()

        # make paths
        img_path = os.path.join(IMG_DIR_PATH, name + ".jpg")
        seg_path = os.path.join(SEG_DIR_PATH, name + ".png")

        # load images
        img = cv2.imread(img_path)
        seg = cv2.imread(seg_path)

        # get a minimum size
        row = img.shape[0]
        col = img.shape[1]

        if row < col:
            min_size = row
            scale = DST_SIZE / float(min_size)
            dst_size = (int(scale * col), DST_SIZE)
        else:
            min_size = col
            scale = DST_SIZE / float(min_size)
            dst_size = (DST_SIZE, int(scale * row))

        # scale images
        dst_img = cv2.resize(img, dst_size)
        dst_seg = cv2.resize(seg, dst_size)

        # crop images
        dst_img = cropper.crop_center_image(dst_img)
        dst_seg = cropper.crop_center_image(dst_seg)
        # 縮小すると色が混ざる。
        # segmentation colorがおかしくなるはず。

        # make output paths
        dst_img_path = os.path.join(DST_IMG_DIR_PATH, name + ".png")
        dst_seg_path = os.path.join(DST_SEG_DIR_PATH, name + ".png")

        # save them
        cv2.imwrite(dst_img_path, dst_img)
        cv2.imwrite(dst_seg_path, dst_seg)

if __name__ == "__main__":
    resize_images(TRAINING_FILE_PATH)
    # resize_images(TESTING_FILE_PATH)

    # check_size(DST_SEG_DIR_PATH)
