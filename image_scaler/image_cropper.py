#!/usr/bin/env python
# coding:utf-8


import cv2
import numpy as np


class ImageCropper(object):
    INITIAL_IMAGE_SIZE = 224
    SCALE_FACTOR = 255

    def __init__(self, in_size):
        self.in_size = in_size

    def crop(self, img, top, left):
        bottom = self.in_size + top
        right = self.in_size + left
        return img[top:bottom, left:right, :].astype(np.float32)

    # test ok
    def crop_center_image(self, image):
        image_row = image.shape[0]
        image_col = image.shape[1]
        if image_col > image_row:
            left = (image_col - ImageCropper.INITIAL_IMAGE_SIZE)/2
            top = 0
        else:
            left = 0
            top = (image_row - ImageCropper.INITIAL_IMAGE_SIZE)/2
        return self.crop(image, top, left)
