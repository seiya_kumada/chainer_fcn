#!/usr/bin/env python
# -*- coding: utf-8 -*-

import chainer
from chainer import Variable
import chainer.links as L
import chainer.functions as F
from add import add2
import numpy as np
import math

class MyFcn16sWithAnySize(chainer.Chain):

    """
    - It takes images with any size as imput
    """
    CLASSES = 21 

    def __init__(self):
        super(MyFcn16sWithAnySize, self).__init__(
            conv1_1=L.Convolution2D(  3,  64, 3, stride=1, pad=1),
            conv1_2=L.Convolution2D( 64,  64, 3, stride=1, pad=1),

            conv2_1=L.Convolution2D( 64, 128, 3, stride=1, pad=1),
            conv2_2=L.Convolution2D(128, 128, 3, stride=1, pad=1),

            conv3_1=L.Convolution2D(128, 256, 3, stride=1, pad=1),
            conv3_2=L.Convolution2D(256, 256, 3, stride=1, pad=1),
            conv3_3=L.Convolution2D(256, 256, 3, stride=1, pad=1),

            conv4_1=L.Convolution2D(256, 512, 3, stride=1, pad=1),
            conv4_2=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv4_3=L.Convolution2D(512, 512, 3, stride=1, pad=1),

            conv5_1=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv5_2=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv5_3=L.Convolution2D(512, 512, 3, stride=1, pad=1),

            score_pool4=L.Convolution2D(512, MyFcn16sWithAnySize.CLASSES, 1, stride=1, pad=0, 
                nobias=True, initialW=MyFcn16sWithAnySize.make_initial_w_zeros(512, MyFcn16sWithAnySize.CLASSES, 1)),
            score_pool5=L.Convolution2D(512, MyFcn16sWithAnySize.CLASSES, 1, stride=1, pad=0, 
                nobias=True, initialW=MyFcn16sWithAnySize.make_initial_w_zeros(512, MyFcn16sWithAnySize.CLASSES, 1)),
        )
        self.train = True 

        # calculate a bilinear interpolation kernel and make a Variable object
        w = MyFcn16sWithAnySize.make_bilinear_interpolation_kernel(MyFcn16sWithAnySize.CLASSES, MyFcn16sWithAnySize.CLASSES, ksize=4)
        self.multi02_bilinear_weights = chainer.Variable(chainer.cuda.to_gpu(w))

        # calculate a bilinear interpolation kernel of the final layer and make a Variable object
        w = MyFcn16sWithAnySize.make_bilinear_interpolation_kernel(MyFcn16sWithAnySize.CLASSES, MyFcn16sWithAnySize.CLASSES, ksize=32)
        self.multi16_bilinear_weights = chainer.Variable(chainer.cuda.to_gpu(w))

    @staticmethod
    def make_initial_w_zeros(in_channels, out_channels, ksize):
        return np.zeros((out_channels, in_channels, ksize, ksize)).astype(np.float32)

    @staticmethod
    def make_bilinear_interpolation_kernel(in_channels, out_channels, ksize):
        # calculate a bilinear interpolation kernel
        factor = (ksize + 1) / 2
        if ksize % 2 == 1:
            center = factor - 1
        else:
            center = factor - 0.5
        og = np.ogrid[:ksize, :ksize]
        k = (1 - abs(og[0] - center) / factor) * (1 - abs(og[1] - center) / factor)
       
        # make a Variable object on GPU
        w = np.zeros((out_channels, in_channels, ksize, ksize)).astype(np.float32)
        w[range(out_channels), range(in_channels), :, :] = k
        return w    

    def __call__(self, x, t):
        
        h = F.relu(self.conv1_1(x))
        h = F.relu(self.conv1_2(h))
        h = F.max_pooling_2d(h, 2, stride=2)

        h = F.relu(self.conv2_1(h))
        h = F.relu(self.conv2_2(h))
        h = F.max_pooling_2d(h, 2, stride=2)
    
        h = F.relu(self.conv3_1(h))
        h = F.relu(self.conv3_2(h))
        h = F.relu(self.conv3_3(h))
        h = F.max_pooling_2d(h, 2, stride=2)
        pool3 = h

        h = F.relu(self.conv4_1(h))
        h = F.relu(self.conv4_2(h))
        h = F.relu(self.conv4_3(h))
        h = F.max_pooling_2d(h, 2, stride=2)
        pool4 = h

        h = F.relu(self.conv5_1(h))
        h = F.relu(self.conv5_2(h))
        h = F.relu(self.conv5_3(h))
        h = F.max_pooling_2d(h, 2, stride=2)
        pool5 = h

        p4 = self.score_pool4(pool4) # 1/16
        self.p4_shape = p4.data.shape

        p5 = self.score_pool5(pool5) # 1/32
        self.p5_shape = p5.data.shape

        row_pad = self.calculate_pad(self.p5_shape[2], self.p4_shape[2], 2, 4)
        col_pad = self.calculate_pad(self.p5_shape[3], self.p4_shape[3], 2, 4)
        
        # deconvolutional layer is fixed to bilinear interpolation
        u5 = F.deconvolution_2d(p5, self.multi02_bilinear_weights, stride=2, pad=(row_pad, col_pad),
                outsize=(self.p4_shape[2], self.p4_shape[3])) # 1/16
        self.u5_shape = u5.data.shape       

        h = add2(p4, u5) # 1/16

        row_pad = self.calculate_pad(h.data.shape[2], t.data.shape[1], 16, 32)
        col_pad = self.calculate_pad(h.data.shape[3], t.data.shape[2], 16, 32)
        
        # deconvolutional layer is fixed to bilinear interpolation
        h = F.deconvolution_2d(h, self.multi16_bilinear_weights, stride=16, pad=(row_pad, col_pad),
                outsize=(t.data.shape[1], t.data.shape[2])) # 1
        self.final_shape = h.data.shape       

        self.loss = F.softmax_cross_entropy(h, t)
        if math.isnan(self.loss.data):
            raise RuntimeError("ERROR in MyFcn16sWithAnySize: loss.data is nan!")
        
        self.accuracy = F.accuracy(h, t, ignore_label=-1)
        if self.train:
            return self.loss
        else:
            self.pred = F.softmax(h)
            return self.pred

    @staticmethod
    def calculate_pad(in_size, out_size, stride, ksize):
        return math.ceil(((stride * in_size - out_size) + (ksize - stride)) / 2.0) 
