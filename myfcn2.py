#!/usr/bin/env python
# -*- coding: utf-8 -*-

import chainer
from chainer import Variable
import chainer.links as L
import chainer.functions as F
from add import add
import numpy as np
import math

class MyFcn2(chainer.Chain):

    """
    myfcn2
    - It takes (224, 224, 3) sized image as imput
    """
    CLASSES = 21 
    IN_SIZE = 224

    def __init__(self):
        super(MyFcn2, self).__init__(
            conv1_1=L.Convolution2D(  3,  64, 3, stride=1, pad=1),
            conv1_2=L.Convolution2D( 64,  64, 3, stride=1, pad=1),

            conv2_1=L.Convolution2D( 64, 128, 3, stride=1, pad=1),
            conv2_2=L.Convolution2D(128, 128, 3, stride=1, pad=1),

            conv3_1=L.Convolution2D(128, 256, 3, stride=1, pad=1),
            conv3_2=L.Convolution2D(256, 256, 3, stride=1, pad=1),
            conv3_3=L.Convolution2D(256, 256, 3, stride=1, pad=1),

            conv4_1=L.Convolution2D(256, 512, 3, stride=1, pad=1),
            conv4_2=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv4_3=L.Convolution2D(512, 512, 3, stride=1, pad=1),

            conv5_1=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv5_2=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv5_3=L.Convolution2D(512, 512, 3, stride=1, pad=1),

            conv_fc6=L.Convolution2D( 512, 4096, 1, stride=1, pad=0),
            conv_fc7=L.Convolution2D(4096, 4096, 1, stride=1, pad=0),
            conv_fc8=L.Convolution2D(4096, MyFcn2.CLASSES, 1, stride=1, pad=0),

            score_pool3=L.Convolution2D(256, MyFcn2.CLASSES, 1, stride=1, pad=0, nobias=True, initialW=MyFcn2.make_initial_w_zeros(256, MyFcn2.CLASSES, 1)),
            score_pool4=L.Convolution2D(512, MyFcn2.CLASSES, 1, stride=1, pad=0, nobias=True, initialW=MyFcn2.make_initial_w_zeros(512, MyFcn2.CLASSES, 1)),

            norm3=L.BatchNormalization(256),
            norm4=L.BatchNormalization(512),
            norm5=L.BatchNormalization(512),
        )
        self.train = True 

        # calculate a bilinear interpolation kernel and make a Variable object
        w = MyFcn2.make_bilinear_interpolation_kernel(MyFcn2.CLASSES, MyFcn2.CLASSES, ksize=4, stride=2, pad=1)
        self.pool4_bilinear_weights = chainer.Variable(chainer.cuda.to_gpu(w))

        # calculate a bilinear interpolation kernel and make a Variable object
        w = MyFcn2.make_bilinear_interpolation_kernel(MyFcn2.CLASSES, MyFcn2.CLASSES, ksize=8, stride=4, pad=2)
        self.conv_fc8_bilinear_weights = chainer.Variable(chainer.cuda.to_gpu(w))
        
        # calculate a bilinear interpolation kernel of the final layer and make a Variable object
        w = MyFcn2.make_bilinear_interpolation_kernel(MyFcn2.CLASSES, MyFcn2.CLASSES, ksize=16, stride=8, pad=4)
        self.fixed_bilinear_weights = chainer.Variable(chainer.cuda.to_gpu(w))

    @staticmethod
    def make_initial_w_zeros(in_channels, out_channels, ksize):
        return np.zeros((out_channels, in_channels, ksize, ksize)).astype(np.float32)

    @staticmethod
    def copy_weights(deconv, ws):
        ps = deconv.namedparams()
        for p in ps:
            if p[0] == "/W": 
                p[1].data = ws

    @staticmethod
    def make_bilinear_interpolation_kernel(in_channels, out_channels, ksize, stride, pad):
        # calculate a bilinear interpolation kernel
        factor = (ksize + 1) / 2
        if ksize % 2 == 1:
            center = factor - 1
        else:
            center = factor - 0.5
        og = np.ogrid[:ksize, :ksize]
        k = (1 - abs(og[0] - center) / factor) * (1 - abs(og[1] - center) / factor)
       
        # make a Variable object on GPU
        w = np.zeros((out_channels, in_channels, ksize, ksize)).astype(np.float32)
        w[range(out_channels), range(in_channels), :, :] = k
        return w    

    def __call__(self, x, t):
        
        h = F.relu(self.conv1_1(x))
        h = F.relu(self.conv1_2(h))
        h = F.max_pooling_2d(h, 2, stride=2)

        h = F.relu(self.conv2_1(h))
        h = F.relu(self.conv2_2(h))
        h = F.max_pooling_2d(h, 2, stride=2)
    
        h = F.relu(self.conv3_1(h))
        h = F.relu(self.conv3_2(h))
        h = F.relu(self.conv3_3(h))
        h = self.norm3(h, test=not self.train)
        h = F.max_pooling_2d(h, 2, stride=2)
        pool3 = h

        h = F.relu(self.conv4_1(h))
        h = F.relu(self.conv4_2(h))
        h = F.relu(self.conv4_3(h))
        h = self.norm4(h, test=not self.train)
        h = F.max_pooling_2d(h, 2, stride=2)
        pool4 = h

        h = F.relu(self.conv5_1(h))
        h = F.relu(self.conv5_2(h))
        h = F.relu(self.conv5_3(h))
        h = self.norm5(h, test=not self.train)
        h = F.max_pooling_2d(h, 2, stride=2)
       
        h = F.dropout(F.relu(self.conv_fc6(h)), train=self.train, ratio=0.5)
        h = F.dropout(F.relu(self.conv_fc7(h)), train=self.train, ratio=0.5)
        h = self.conv_fc8(h) 
        conv_fc8 = h

        p3 = self.score_pool3(pool3)
        self.p3_shape = p3.data.shape   

        p4 = self.score_pool4(pool4)
        self.p4_shape = p4.data.shape

        # deconvolutional layer is fixed to bilinear interpolation
        u4 = F.deconvolution_2d(p4, self.pool4_bilinear_weights, stride=2, pad=1)
        self.u4_shape = u4.data.shape      

        # deconvolutional layer is fixed to bilinear interpolation
        u8 = F.deconvolution_2d(conv_fc8, self.conv_fc8_bilinear_weights, stride=4, pad=2)
        self.u8_shape = u8.data.shape       

        h = add(p3, u4, u8)

        # deconvolutional layer is fixed to bilinear interpolation
        h = F.deconvolution_2d(h, self.fixed_bilinear_weights, stride=8, pad=4)
        self.final_shape = h.data.shape       

        self.loss = F.softmax_cross_entropy(h, t)
        if math.isnan(self.loss.data):
            raise RuntimeError("ERROR in MyFcn2: loss.data is nan!")
        
        self.accuracy = self.calculate_accuracy(h, t)
        if self.train:
            return self.loss
        else:
            self.pred = F.softmax(h)
            return self.pred

    def calculate_accuracy(self, predictions, truths):
        gpu_predictions = predictions.data
        gpu_truths = truths.data

        cpu_predictions = chainer.cuda.to_cpu(gpu_predictions)
        cpu_truths = chainer.cuda.to_cpu(gpu_truths)

        # we want to exclude labels with -1
        mask = cpu_truths != -1

        # reduce values along classe axis
        reduced_cpu_preditions = np.argmax(cpu_predictions, axis=1)

        # mask
        masked_reduced_cpu_preditions = reduced_cpu_preditions[mask]
        masked_cpu_truths = cpu_truths[mask]

        s = (masked_reduced_cpu_preditions == masked_cpu_truths).mean()
        return s






